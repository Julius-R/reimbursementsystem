Employee Reimbursement System

Employee Reimbursement System provides services for managing employees' reimbursement.

Technologies Utilized

    Java
    Servlets
    Maven
    AWS RDS
    PostgreSQL
    JavaScript
    HTML
    SASS

Features

    As a user, I can login
    As a user, I can logout

    As an employee, I can see my reimbursement ticket history(pending, approved, denied)
    As an employee, I can submit a new reimbursement request ticket

    As a finance manager, I can see all employees' reimbursement ticket history(pending, approved, denied)
    As a finance manager, I can resolve reimbursement requests by approval or denial

Getting Started

    git clone https://gitlab.com/Julius-R/reimbursementsystem
    Add Apache Tomcat to the Servers in the application's workspace
    Add the application(project) to the Tomcat server

Usage

    Login with a user's credential that was pre-populated in the database.
    Depending on the user account type, the app will be redirected to either a Finance Manager's account or an Eomployee's account.
    As an employee, users can see all of their past reimbursement requests and submit a new reimbursement request.
    As a finance manager, users can see all employees' past and currently pending reimbursement request, and resolve it by either denying or accepting the request.
    Both type of account users can logout.
