package com.bittunes.model;

public class Reimburesment {

    private int reimbId;
    private String reimbAmount;
    private String reimbSubmitted;
    private String reimbResolved;
    private String reimbDescription;
    private String reimbAuthor;
    private String reimbResolver;
    private int reimbStatusId;
    private int reimbTypeId;
    private String reimbStatus;
    private String reimbType;

    public Reimburesment() {
    }

    public Reimburesment(int reimbId, String reimbAmount, String reimbSubmitted, String reimbResolved,
                         String reimbDescription, String reimbAuthor, String reimbResolver, int reimbStatusId, int reimbTypeId, String reimbStatus, String reimbType) {
        this.reimbId = reimbId;
        this.reimbAmount = reimbAmount;
        this.reimbSubmitted = reimbSubmitted;
        this.reimbResolved = reimbResolved;
        this.reimbDescription = reimbDescription;
        this.reimbAuthor = reimbAuthor;
        this.reimbResolver = reimbResolver;
        this.reimbStatusId = reimbStatusId;
        this.reimbTypeId = reimbTypeId;
        this.reimbStatus = reimbStatus;
        this.reimbType = reimbType;
    }



    public int getReimbId() {
        return reimbId;
    }

    public void setReimbId(int reimbId) {
        this.reimbId = reimbId;
    }

    public String getReimbAmount() {
        return reimbAmount;
    }

    public void setReimbAmount(String reimbAmount) {
        this.reimbAmount = reimbAmount;
    }

    public String getReimbSubmitted() {
        return reimbSubmitted;
    }

    public void setReimbSubmitted(String reimbSubmitted) {
        this.reimbSubmitted = reimbSubmitted;
    }

    public String getReimbResolved() {
        return reimbResolved;
    }

    public void setReimbResolved(String reimbResolved) {
        this.reimbResolved = reimbResolved;
    }

    public String getReimbDescription() {
        return reimbDescription;
    }

    public void setReimbDescription(String reimbDescription) {
        this.reimbDescription = reimbDescription;
    }

    public String getReimbAuthor() {
        return reimbAuthor;
    }

    public void setReimbAuthor(String reimbAuthor) {
        this.reimbAuthor = reimbAuthor;
    }

    public String getReimbResolver() {
        return reimbResolver;
    }

    public void setReimbResolver(String reimbResolver) {
        this.reimbResolver = reimbResolver;
    }

    public int getReimbStatusId() {
        return reimbStatusId;
    }

    public void setReimbStatusId(int reimbStatusId) {
        this.reimbStatusId = reimbStatusId;
    }

    public int getReimbTypeId() {
        return reimbTypeId;
    }

    public void setReimbTypeId(int reimbTypeId) {
        this.reimbTypeId = reimbTypeId;
    }

    public String getReimbStatus() {
        return reimbStatus;
    }

    public void setReimbStatus(String reimbStatus) {
        this.reimbStatus = reimbStatus;
    }

    public String getReimbType() {
        return reimbType;
    }

    public void setReimbType(String reimbType) {
        this.reimbType = reimbType;
    }

    @Override
    public String toString() {
        return "Reimburesment{" +
                "reimbId=" + reimbId +
                ", reimbAmount=" + reimbAmount +
                ", reimbSubmitted='" + reimbSubmitted + '\'' +
                ", reimbResolved='" + reimbResolved + '\'' +
                ", reimbDescription='" + reimbDescription + '\'' +
                ", reimbAuthor='" + reimbAuthor + '\'' +
                ", reimbResolver='" + reimbResolver + '\'' +
                ", reimbStatusId=" + reimbStatusId +
                ", reimbTypeId=" + reimbTypeId +
                '}';
    }
}
