package com.bittunes.json;

import com.bittunes.model.User;
import com.bittunes.service.ReimbursementSreviceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReimbursementJson {

    public static void updateReimbursements(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException,
            IOException{
        User loggedIn = (User) request.getSession().getAttribute("login");
        int status = Integer.parseInt(request.getParameter("status"));
        int id = Integer.parseInt(request.getParameter("id"));
//        System.out.println("Status: " + status + " Id: " + id);
        ReimbursementSreviceImpl.getReimbursementSreviceImpl().updateStatus(loggedIn, status, id);
    }

    public static void createReimbursement(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException{
        User loggedIn = (User) request.getSession().getAttribute("login");
        float amt = Float.parseFloat(request.getParameter("amt"));
        String reason = request.getParameter("reason");
        int type = Integer.parseInt(request.getParameter("type"));
        System.out.println("Amount: " + amt + " Reason: " + reason + " Type: " + type);
        System.out.println(loggedIn.getUserId() + ": User Id");
        ReimbursementSreviceImpl.getReimbursementSreviceImpl().createReimbursement(amt, reason, loggedIn, type);
//        response.getWriter().write(new ObjectMapper().writeValueAsString(loggedIn));
    }
}
