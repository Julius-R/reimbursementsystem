package com.bittunes.json;

import com.bittunes.model.User;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class UserJson {
    public static void displayUser(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException{
        User loggedIn = (User) request.getSession().getAttribute("login");
        System.out.println(loggedIn);
        response.getWriter().write(new ObjectMapper().writeValueAsString(loggedIn));
    }
}
