package com.bittunes.json;

import com.bittunes.model.Reimburesment;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JsonRequestHelper {

    public static void process(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
        System.out.println(request.getRequestURI());
        // Redirect if there is no session
        if(request.getSession(false) == null){
            response.sendRedirect("/BitTunes_war_exploded/login.html");
        } else {
            switch (request.getRequestURI()) {
                case "/BitTunes_war_exploded/json/user":
                    System.out.println("In user");
                    UserJson.displayUser(request, response);
                    break;
                case "/BitTunes_war_exploded/json/create":
                    ReimbursementJson.createReimbursement(request, response);
                    break;
                case "/BitTunes_war_exploded/json/update":
                    ReimbursementJson.updateReimbursements(request, response);
                    break;
                default:
                    System.out.println("issue!!!");
                    // Create an error class that shows a message if request yields nothing
                    // BadAskJson badAsk = new BadAskJson(-1, null, -1, -1);

                    //response.getWriter().write(new ObjectMapper().writeValueAsString());


            }
        }

    }

}
