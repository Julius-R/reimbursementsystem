package com.bittunes.service;

import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;

import java.util.List;

public interface ReimbursementService {
    // Display reimbursements
    public List<Reimburesment> getAllReimbursements();
    public List<Reimburesment> getAllReimbursementsForUser(User user);

    // Create a reimbursement
    public void createReimbursement(float amt, String reason, User user, int type);

    // Alter status of the reimbursement
    public void updateStatus(User user, int status, int id);
}
