package com.bittunes.service;

import com.bittunes.model.User;

public interface UserService {
    public User checkUserNameAndPassword(String username, String password);
}
