package com.bittunes.service;

import com.bittunes.dao.UserDaoImpl;
import com.bittunes.model.User;

public class UserServiceImpl implements UserService{

    private static UserServiceImpl userServiceImpl;

    private UserServiceImpl(){}

    public static UserServiceImpl getUserServiceImpl(){
        if(userServiceImpl == null){
            return userServiceImpl = new UserServiceImpl();
        }

        return userServiceImpl;
    }

    @Override
    public User checkUserNameAndPassword(String username, String password) {
        return UserDaoImpl.getUserDaoImpl().checkUserNameAndPassword(username, password);
    }
}
