package com.bittunes.service;

import com.bittunes.dao.ReimbursementDaoImpl;
import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;

import java.util.List;

public class ReimbursementSreviceImpl implements ReimbursementService{

    private static ReimbursementSreviceImpl reimbursementSreviceImpl;

    private ReimbursementSreviceImpl() {}

    public static ReimbursementSreviceImpl getReimbursementSreviceImpl(){
        if(reimbursementSreviceImpl == null){
            reimbursementSreviceImpl = new ReimbursementSreviceImpl();
        }

        return reimbursementSreviceImpl;
    }

    @Override
    public List<Reimburesment> getAllReimbursements() {
        return ReimbursementDaoImpl.getReimbursementDaoImpl().getAllReimbursements();
    }

    @Override
    public List<Reimburesment> getAllReimbursementsForUser(User user) {
        return ReimbursementDaoImpl.getReimbursementDaoImpl().getAllReimbursementsForUser(user);
    }

    @Override
    public void createReimbursement(float amt, String reason, User user, int type) {
        ReimbursementDaoImpl.getReimbursementDaoImpl().createReimbursement(amt, reason, user, type);
    }

    @Override
    public void updateStatus(User user, int status, int id) {
        ReimbursementDaoImpl.getReimbursementDaoImpl().updateStatus(user, status, id);
    }
}
