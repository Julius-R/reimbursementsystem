package com.bittunes.dao;

import com.bittunes.model.User;

public interface UserDao {
    // What can users do

    // Validation of login
    public User checkUserNameAndPassword(String username, String password);

}
