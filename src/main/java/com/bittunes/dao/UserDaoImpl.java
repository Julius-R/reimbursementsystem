package com.bittunes.dao;

import com.bittunes.model.User;
import com.bittunes.util.ConnectionUtil;
import com.bittunes.util.LogUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao{

    private static UserDaoImpl userDaoImpl;

    public UserDaoImpl(){

    }

    public static UserDaoImpl getUserDaoImpl() {
        if(userDaoImpl == null) {
            userDaoImpl = new UserDaoImpl();
        }

        return userDaoImpl;
    }

    @Override
    public User checkUserNameAndPassword(String username, String password) {
        try(Connection conn = ConnectionUtil.getConnection()){

            String command = "SELECT users_id , users_email, users_username, users_firstname, users_lastname, role_name FROM users u\n" +
                    "INNER JOIN user_roles A ON A.role_id = u.role_id WHERE users_email  = ? AND " +
                    "users_password =  crypt(?,users_password);";

            PreparedStatement statement = conn.prepareStatement(command);
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet result = statement.executeQuery();

            while(result.next()){
                return new User(
                        result.getInt(1),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getString(2),
                        result.getString(6)
                        );
            }

            LogUtil.logger.info(result.getString(2) + " Successfully logged in.");



        } catch (SQLException e) {
            LogUtil.logger.warn("Error logging in: " + e);
        }

        return new User();
    }
}
