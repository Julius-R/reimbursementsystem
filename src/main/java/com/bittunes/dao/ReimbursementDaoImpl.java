package com.bittunes.dao;

import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;
import com.bittunes.util.ConnectionUtil;
import com.bittunes.util.LogUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ReimbursementDaoImpl implements ReimburesmentDao{

    private static ReimbursementDaoImpl reimbursementDaoImpl;

    public ReimbursementDaoImpl(){}

    public static ReimbursementDaoImpl getReimbursementDaoImpl(){
        if(reimbursementDaoImpl == null){
            reimbursementDaoImpl = new ReimbursementDaoImpl();
        }

        return reimbursementDaoImpl;
    }

    public void createModel(){

    }

    @Override
    public List<Reimburesment> getAllReimbursements() {
        List<Reimburesment> reimbursementList = new ArrayList<>();
        try(Connection connection = ConnectionUtil.getConnection()){

            String sql = "SELECT r.reimb_id,\n" +
                    "\tr.reimb_amount, r.reimb_submitted,\n" +
                    "\tr.reimb_resolved, r.reimb_description,\n" +
                    "\tur.users_firstname, us.users_firstname AS\n" +
                    "\tresolved_by, r.reimb_status_id, r.reimb_type_id, rs.reimb_status, rt.reimb_type\n" +
                    "FROM reimbursements r INNER JOIN reimbursement_status rs ON rs.reimb_status_id = r" +
                    ".reimb_status_id INNER JOIN reimbursement_type rt\n" +
                    "ON rt.reimb_type_id = r.reimb_type_id \n" +
                    "INNER JOIN  users ur \n" +
                    "ON ur.users_id = r.reimb_author LEFT JOIN users us ON us.users_id = r.reimb_resolver";;

            PreparedStatement ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery(); //<--query not update

            while(rs.next()) {
                reimbursementList.add(new Reimburesment(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10),
                        rs.getString(11)
                ));
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return reimbursementList;

    }


    @Override
    public List<Reimburesment> getAllReimbursementsForUser(User user) {
        List<Reimburesment> reimbursementList = new ArrayList<>();
        try(Connection connection = ConnectionUtil.getConnection()){

            String sql = "SELECT r.reimb_id, r.reimb_amount,\n" +
                    "\tr.reimb_submitted, r.reimb_resolved,\n" +
                    "\tr.reimb_description, ur.users_firstname,\n" +
                    "\tus.users_firstname AS resolved_by, r.reimb_status_id,\n" +
                    "\tr.reimb_type_id, rs.reimb_status,\n" +
                    "\trt.reimb_type FROM reimbursements r INNER JOIN\n" +
                    "reimbursement_status rs ON rs.reimb_status_id = r.reimb_status_id INNER JOIN reimbursement_type rt\n" +
                    "ON rt.reimb_type_id = r.reimb_type_id \n" +
                    "INNER JOIN  users ur ON ur.users_id = r.reimb_author\n" +
                    "LEFT  JOIN users us ON us.users_id = r.reimb_resolver WHERE r.reimb_author = ?";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, user.getUserId());
            ResultSet rs = ps.executeQuery(); //<--query not update

            while(rs.next()) {
                reimbursementList.add(new Reimburesment(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10),
                        rs.getString(11)
                ));
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return reimbursementList;

    }

    @Override
    public boolean createReimbursement(float amt, String reason, User user, int type) {
        try(Connection connection = ConnectionUtil.getConnection()){
            String command = "INSERT INTO reimbursements (\n" +
                    "\treimb_amount,\n" +
                    "\treimb_submitted,\n" +
                    "\treimb_description,\n" +
                    "\treimb_author,\n" +
                    "\treimb_status_id,\n" +
                    "\treimb_type_id\n" +
                    ") values(\n" +
                    "?,\n" +
                    "CURRENT_TIMESTAMP,\n" +
                    "?,\n" +
                    "?,\n" +
                    "2,\n" +
                    "?\n" +
                    ")";
            PreparedStatement statement = connection.prepareStatement(command);
            statement.setBigDecimal(1, BigDecimal.valueOf(amt));
            statement.setString(2, reason);
            statement.setInt(3, user.getUserId());
            statement.setInt(4, type);

            if(statement.executeUpdate() > 0){
                LogUtil.logger.info("User: " + user.getFirstName() + " " + user.getLastName() + " created a " +
                        "reimbursement.");
                return true;
            }

        } catch (SQLException e){
            e.printStackTrace();
            LogUtil.logger.warn("Error adding reimbursement: ", e);
        }


        return false;
    }

    @Override
    public boolean updateStatus(User user, int status, int id) {
        try(Connection connection = ConnectionUtil.getConnection()){
            String command = "UPDATE reimbursements\n" +
                    "SET reimb_status_id = ?, reimb_resolved = current_timestamp, reimb_resolver = ?\n" +
                    "WHERE reimb_id = ? ";
            PreparedStatement statement = connection.prepareStatement(command);

            statement.setDouble(1, status);
            statement.setInt(2, user.getUserId());
            statement.setInt(3, id);

            if(statement.executeUpdate() > 0){
                LogUtil.logger.info(user.getFirstName() + " " + user.getLastName() + " updated a reimbursement's " +
                        "status");
                return true;
            }

        } catch (SQLException e){
            LogUtil.logger.warn("Error updating reimbursement: ", e);
        }
        return false;
    }
}
