package com.bittunes.dao;

import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;

import java.util.List;

public interface ReimburesmentDao {
    // What can be done with reimbursements?

    // Display reimbursements
    public List<Reimburesment> getAllReimbursements();
    public List<Reimburesment> getAllReimbursementsForUser(User user);

    // Create a reimbursement
    public boolean createReimbursement(float amt, String reason, User user, int type);

    // Alter status of the reimbursement
    public boolean updateStatus(User user, int status, int id);

}
