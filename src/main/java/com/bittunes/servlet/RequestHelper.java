package com.bittunes.servlet;

import com.bittunes.controller.HomeController;
import com.bittunes.controller.LoginController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestHelper {
    public static String process(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("\t\tIn RequestHelper");
        System.out.println(request.getRequestURI());


        switch(request.getRequestURI()) {
            case "/BitTunes_war_exploded/forwarding/login":
                System.out.println("case1");
                return LoginController.login(request);
            case "/BitTunes_war_exploded/forwarding/logout":
                System.out.println("logging out");
                return LoginController.logout(request);
            case "/BitTunes_war_exploded/forwarding/home/employee":
                System.out.println("case 2");
                return HomeController.employee(request, response);
            case "/BitTunes_war_exploded/forwarding/home/finance":
                System.out.println("case 3");
                return HomeController.manager(request, response);
            default:
                System.out.println("bad checkpoint");
                return "/resources/html/badlogin.html";
        }
    }

}
