package com.bittunes.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomeController {
    public static String employee(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getSession(false) == null) {
            response.sendRedirect("/BitTunes_war_exploded/login.html");
            return "";
        } else {
            response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
            return "/resources/html/employee.html";
        }
    }
    public static String manager(HttpServletRequest request,HttpServletResponse response) throws IOException {
        if (request.getSession(false) == null) {
            response.sendRedirect("/BitTunes_war_exploded/login.html");
            return "";
        } else {
            response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
            return "/resources/html/finance.html";
        }
    }
}
