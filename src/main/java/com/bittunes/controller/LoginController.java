package com.bittunes.controller;

import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;
import com.bittunes.service.ReimbursementSreviceImpl;
import com.bittunes.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class LoginController {
    public static String login(HttpServletRequest request) {

        if(!request.getMethod().equals("POST")) {
            return "/login.html";
        }


        String loginPath = "";
        //extracting the form data
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User loginUser = UserServiceImpl.getUserServiceImpl().checkUserNameAndPassword(email, password);
        //check to see if the user has the correct username and password
        if(loginUser.getUserId() == 0 && loginUser.getUsername() == null) {
            //you'll actually go to the database to get the credentials, don't hardcode them
            return "/forwarding/incorrectcredentials";
        }else {


            switch (loginUser.getRole()){
                case "Finance M":
                    List<Reimburesment> allList =
                            ReimbursementSreviceImpl.getReimbursementSreviceImpl().getAllReimbursements();

                    loginUser.setReimbursementList(allList);
                    loginPath = "/forwarding/home/finance";
                    break;
                case "Employee":
                    List<Reimburesment> allListUser =
                            ReimbursementSreviceImpl.getReimbursementSreviceImpl().getAllReimbursementsForUser(loginUser);
                    System.out.println(allListUser);
                    loginUser.setReimbursementList(allListUser);
                    loginPath = "/forwarding/home/employee";
                    break;
            }

            request.getSession().setAttribute("login", loginUser);

            return loginPath;
        }
    }
    public static String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        System.out.println("Redirext");
        return "http://localhost:8080/BitTunes_war_exploded/login.html";
    }
}
