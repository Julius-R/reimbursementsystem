let pendingR =[], approvedR = [], deniedR = [], allR = [];


fetch("http://localhost:8080/BitTunes_war_exploded/json/user")
    .then(res => res.json())
    .then(data => {

        filterData(data)

        allR = data.reimbursementList;
        displayData(allR)
        showStats()
        document.title = `Welcome Back, ${data.firstName}`;
        document.querySelector("#user").innerText = data.firstName;
    }).then((data)=>{
    console.log(data)
}).catch(err => console.log(err))


const filterData = (arr) => {
    arr.reimbursementList.filter(reimbursement => {
        if(reimbursement.reimbStatus == "DENIED"){
            deniedR.push(reimbursement);
        }else if(reimbursement.reimbStatus == "APPROVED"){
            approvedR.push(reimbursement);
        } else {
            pendingR.push(reimbursement);
        }
    })
}

const displayData = (arr) => {
    document.querySelector(".list-group").innerHTML = "";
    arr.forEach(reimbursement => {
        document.querySelector(".list-group").insertAdjacentHTML('beforeend', `
<section class="list-group-item list-group-item-action">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">${reimbursement.reimbAmount}</h5>
      <small class="text-muted">Submitted: ${new Date(reimbursement.reimbSubmitted).toLocaleString()}</small>
    </div>
    <p class="mb-1">${reimbursement.reimbDescription}</p>
    
    <small class="text-muted">Status: ${reimbursement.reimbStatus == "PENDING" ? "PENDING" : reimbursement.reimbStatus + " on " + new Date(reimbursement.reimbResolved).toLocaleString() + " by " + reimbursement.reimbResolver} .</small>
  </section>`)
    })
}

const showStats = () => {
    document.querySelector(".stats").insertAdjacentHTML('beforeend',
        `<li>Denied <span id="Denied">${deniedR.length}</span></li>
            <li>Approved <span id="Approved">${approvedR.length}</span></li>
            <li>Pending <span id="Pending">${pendingR.length}</span></li>`)
}

document.querySelector("#chooseView").addEventListener('change', function(e){
    switch(e.target.value){
        case "All":
            displayData(allR);
            break;
        case "Approved":
            displayData(approvedR);
            break;
        case "Pending":
            displayData(pendingR);
            break;
        case "Denied":
            displayData(deniedR);
            break;
            deault:
                displayData(allR);
    }
})

document.querySelector("#submitReim").onsubmit = function(e){
    e.preventDefault();
}

document.querySelector("#submitReimbtn").addEventListener('click', function(){
    let a = document.querySelector("#exampleFormControlInput1").value;
    let b = document.querySelector("#exampleFormControlTextarea1").value;
    let c = document.querySelector(".form-select").value;
    let myModalDos = new bootstrap.Modal(document.getElementById('myModalDos'))
    let myModal = new bootstrap.Modal(document.getElementById('myModal'))
    if (/^\s*[0-9][.0-9]*$/.test(a)
        &&
        /^\s*[0-9a-zA-Z_.][_.0-9a-zA-Z !@'-]*$/.test(b)) {
        fetch(`http://localhost:8080/BitTunes_war_exploded/json/create?amt=${a}&reason=${b}&type=${c}`)
            .then(data => {
                myModalDos.show();
                document.querySelector("#submitReim").reset();
            })
            .catch(err => console.log(err))

    }
    else {
        myModal.show();
    }
})