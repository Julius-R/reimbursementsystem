let pendingR =[], approvedR = [], deniedR = [], allR = [];
let pendingArea = document.querySelector("#pendingSubmissions");
let allArea = document.querySelector("#allSubmissions");
let refresh = document.querySelector("#refresh")
refresh.addEventListener("click", () => {
    location.reload();
})
const filterData = (arr) => {
    arr.reimbursementList.filter(reimbursement => {
        if(reimbursement.reimbStatus == "DENIED"){
            deniedR.push(reimbursement);
        }else if(reimbursement.reimbStatus == "APPROVED"){
            approvedR.push(reimbursement);
        } else {
            pendingR.push(reimbursement);
        }
    })
}

document.querySelector("#chooseView").addEventListener('change', function(e){
    switch(e.target.value){
        case "All":
            domControl(allArea,allR, "allArea");
            break;
        case "Approved":
            domControl(allArea,approvedR, "allArea");
            break;
        case "Pending":
            domControl(allArea,pendingR, "allArea");
            break;
        case "Denied":
            domControl(allArea,deniedR, "allArea");
            break;
    }
})







function domControl(location, arr, area) {
    console.log(location)
    if(area != "allArea"){
        location.innerHTML = "";
        arr.map(reimbursement => {
            location.insertAdjacentHTML('beforeend',`
  <section class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
										  <h5 class="mb-1">${reimbursement.reimbAmount}</h5>
										  <small class="text-muted">Submitted: ${new Date(reimbursement.reimbSubmitted).toLocaleString()}</small>
										</div>
										<p class="mb-1">${reimbursement.reimbDescription}</p>
										<small class="text-muted">Submitted By: ${reimbursement.reimbAuthor}</small>
									<section class="submit-area" data-id=${reimbursement.reimbId}>
										<button id="acceptBtn"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-patch-check" viewBox="0 0 16 16">
											<path fill-rule="evenodd" d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
											<path d="M10.273 2.513l-.921-.944.715-.698.622.637.89-.011a2.89 2.89 0 0 1 2.924 2.924l-.01.89.636.622a2.89 2.89 0 0 1 0 4.134l-.637.622.011.89a2.89 2.89 0 0 1-2.924 2.924l-.89-.01-.622.636a2.89 2.89 0 0 1-4.134 0l-.622-.637-.89.011a2.89 2.89 0 0 1-2.924-2.924l.01-.89-.636-.622a2.89 2.89 0 0 1 0-4.134l.637-.622-.011-.89a2.89 2.89 0 0 1 2.924-2.924l.89.01.622-.636a2.89 2.89 0 0 1 4.134 0l-.715.698a1.89 1.89 0 0 0-2.704 0l-.92.944-1.32-.016a1.89 1.89 0 0 0-1.911 1.912l.016 1.318-.944.921a1.89 1.89 0 0 0 0 2.704l.944.92-.016 1.32a1.89 1.89 0 0 0 1.912 1.911l1.318-.016.921.944a1.89 1.89 0 0 0 2.704 0l.92-.944 1.32.016a1.89 1.89 0 0 0 1.911-1.912l-.016-1.318.944-.921a1.89 1.89 0 0 0 0-2.704l-.944-.92.016-1.32a1.89 1.89 0 0 0-1.912-1.911l-1.318.016z"/>
										  </svg>
											Approve</button>
										<button id="denyBtn"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-patch-exclamation" viewBox="0 0 16 16">
											<path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
											<path d="M10.273 2.513l-.921-.944.715-.698.622.637.89-.011a2.89 2.89 0 0 1 2.924 2.924l-.01.89.636.622a2.89 2.89 0 0 1 0 4.134l-.637.622.011.89a2.89 2.89 0 0 1-2.924 2.924l-.89-.01-.622.636a2.89 2.89 0 0 1-4.134 0l-.622-.637-.89.011a2.89 2.89 0 0 1-2.924-2.924l.01-.89-.636-.622a2.89 2.89 0 0 1 0-4.134l.637-.622-.011-.89a2.89 2.89 0 0 1 2.924-2.924l.89.01.622-.636a2.89 2.89 0 0 1 4.134 0l-.715.698a1.89 1.89 0 0 0-2.704 0l-.92.944-1.32-.016a1.89 1.89 0 0 0-1.911 1.912l.016 1.318-.944.921a1.89 1.89 0 0 0 0 2.704l.944.92-.016 1.32a1.89 1.89 0 0 0 1.912 1.911l1.318-.016.921.944a1.89 1.89 0 0 0 2.704 0l.92-.944 1.32.016a1.89 1.89 0 0 0 1.911-1.912l-.016-1.318.944-.921a1.89 1.89 0 0 0 0-2.704l-.944-.92.016-1.32a1.89 1.89 0 0 0-1.912-1.911l-1.318.016z"/>
										  </svg> Deny</button>
									</section>
									  </section>
  `)
        })
        let acceptBtn = document.querySelectorAll("#acceptBtn");
        let denyBtn = document.querySelectorAll("#denyBtn");
        acceptBtn.forEach(btn => {
            btn.addEventListener('click', function(){
                console.log(this.parentNode.dataset.id)
                updateData(this.parentNode.dataset.id, 1)
                this.parentNode.parentNode.remove()
            })
        })

        denyBtn.forEach(btn => {
            btn.addEventListener('click', function(){
                console.log(this.parentNode.dataset.id)
                updateData(this.parentNode.dataset.id, 3)
                this.parentNode.parentNode.remove()
            })
        })
    } else {
        location.innerHTML = "";
        arr.map(reimbursement => {
            location.insertAdjacentHTML('beforeend',`
  <section class="list-group-item list-group-item-action">
  <div class="d-flex w-100 justify-content-between">
    <h5 class="mb-1">${reimbursement.reimbAmount}</h5>
    <small class="text-muted">Submitted: ${new Date(reimbursement.reimbSubmitted).toLocaleString()}</small>
  </div>
  <p class="mb-1">${reimbursement.reimbDescription}</p>
  
  <small class="text-muted">Status: ${reimbursement.reimbStatus == "PENDING" ? "PENDING" : reimbursement.reimbStatus + " on " + new Date(reimbursement.reimbResolved).toLocaleString() + " by " + reimbursement.reimbResolver} .</small>
</section>
  `)
        })
    }
}

function updateData(id, status){
    fetch(`http://localhost:8080/BitTunes_war_exploded/json/update?status=${status}&id=${id}`)
        .catch(err => console.log(err))

}

function displayData(){
    fetch("http://localhost:8080/BitTunes_war_exploded/json/user")
        .then(res => res.json())
        .then(data => {
            console.log(data);

            filterData(data)
            allR = data.reimbursementList;
            document.title = `Welcome Back, ${data.firstName}`;
            document.querySelector("#user").innerText = data.firstName;

            domControl(allArea,data.reimbursementList, "allArea");
            domControl(pendingArea,pendingR, "pendingArea");



        }).then(()=>{

    })
        .catch(err => console.log(err))
}

displayData();