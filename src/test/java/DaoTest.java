import com.bittunes.dao.ReimburesmentDao;
import com.bittunes.dao.ReimbursementDaoImpl;
import com.bittunes.dao.UserDao;
import com.bittunes.dao.UserDaoImpl;
import com.bittunes.model.Reimburesment;
import com.bittunes.model.User;
import org.junit.*;
import static org.junit.Assert.assertEquals;

import java.util.List;

@FixMethodOrder
public class DaoTest {

    ReimburesmentDao reimburesmentDao = new ReimbursementDaoImpl();
    UserDao userDao = new UserDaoImpl();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testReadAllReimbs() {
        List<Reimburesment> list = reimburesmentDao.getAllReimbursements();
        int listSize = list.size();

        assertEquals("Testing if array size is correct", 25 ,listSize);
    }

    @Test
    public void testUsernameAndPassword() {

        User user = UserDaoImpl.getUserDaoImpl().checkUserNameAndPassword("jrobin@bittunes.com", "Gogo12");

        boolean userExists = false;
        if(user.getUsername() != null)
            userExists = true;

        assertEquals("Testing if user is returned", true ,userExists);
    }

    @Test
    public void testUsernameAndPasswordNull() {

        User user = UserDaoImpl.getUserDaoImpl().checkUserNameAndPassword("jrobin@bittunes", "Gogo12");

        boolean userExists = false;
        if(user.getUsername() != null)
            userExists = true;

        assertEquals("Testing if user is not returned", false ,userExists);
    }
@Test
    public void testReadAllReimbsFromAUser() {
        User user = UserDaoImpl.getUserDaoImpl().checkUserNameAndPassword("kivers@bittunes.com", "Domo_arigato");

    List<Reimburesment> list = reimburesmentDao.getAllReimbursementsForUser(user);
        int listSize = list.size();

        assertEquals("Testing if array contains correct size for a finance user", 7 ,listSize);
    }

    @Test
    public void testReadAllReimbsFromAnEmployee() {
        User user = UserDaoImpl.getUserDaoImpl().checkUserNameAndPassword("jrobin@bittunes.com", "Gogo12");

        List<Reimburesment> list = reimburesmentDao.getAllReimbursementsForUser(user);
        int listSize = list.size();

        assertEquals("Testing if array contains correct size for an employee", 14 ,listSize);
    }
/*
/*

    @Test
    public void testReadAllReimbs() {
        List<Reimburesment> list = reimburesmentDao.getAllReimbursements();
        int listSize = list.size();

        assertEquals("Testing if array sizze is correct", 16 ,listSize);
    }

*/
}
